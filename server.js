var express = require('express');
var fs = require('fs');
var server = express();

server.use(express.static(__dirname + '/public'));

server.get('/', function (req, res) {
	res.sendFile( __dirname + '/views/index.html' );
});


var restaurants = [];
var contendors = [];

var clearVotes = function(){
	restaurants.forEach(function(restaurant){
		restaurant.voted.length = 0;
	});
	
	contendors = [];
}

var loadRestaurants = function(){
	// Read from the file
	var data = fs.readFileSync("restaurants", "utf8");

	// Split the || delimited file
	restaurants = JSON.parse(data);
	
	
	// Sort restaurants alphabetically
	restaurants.sort(function(a,b){ 
		if(a.name > b.name){
			return 1;
		}
		if(a.name < b.name){
			return -1;
		}
		return 0;
	});
}

loadRestaurants();



/**********************
 * Restaurant methods
 **********************/
 
server.get('/api/Restaurants/getAll', function (req, res) {
	res.send(restaurants);
});

server.post('/api/Restaurants/create', function(req, res){
	restaurants.push(req.body);
	
	res.send({success: true});
})

server.delete('/api/Restaurants/delete', function(req, res){
	var deleted = false;
	
	restaurants.forEach(function(restaurant){
		if(restaurant.name == req.query.restaurantName){
			restaurants.splice(restaurants.indexOf(restaurant), 1);
			deleted = true;
		}
	})
	
	res.send({success: deleted});
})

server.post('/api/Restaurants/save', function(req, res){
	fs.writeFile("restaurants", JSON.stringify(restaurants), function(err) {
			if(err) {
				return console.log(err);
			}

			console.log("The file was saved!");
		}); 
})



/****************
 * Vote methods
 ****************/
 
server.post('/api/Votes/vote', function(req, res){
	var voted = false;
	
	restaurants.forEach(function(restaurant){
		if(restaurant.name == req.query.restaurantName){
			if(restaurant.voted.indexOf(req.query.voter) == -1){
				restaurant.voted.push(req.query.voter);
				voted = true;
			}
		}
	});
	//console.log('Vote', req.query.restaurantName, voted);
	res.send({success: voted});
})

server.post('/api/Votes/revoke', function(req, res){
	var revoked = false;
	
	restaurants.forEach(function(restaurant){
		if(restaurant.name == req.query.restaurantName){
			if(restaurant.voted.indexOf(req.query.voter) > -1){
				restaurant.voted.splice(restaurant.voted.indexOf(req.query.voter), 1);
				revoked = true;
			}
		}
	})
	//console.log('Revoke', req.query.restaurantName, revoked);
	res.send({success: revoked});
})

/*server.post('/api/Votes/finalize', function(req, res){
	var restaurantTally = restaurants.sort(function(a, b){
			return b.voted.length - a.voted.length;
		});

	// Check the sorted list, if there is more than 1 restaurant, just compare the first two to see if a tie exists
	if(restaurantTally.length > 2 && restaurantTally[0].voted.length == restaurantTally[1].voted.length){
		tieBreakerState = true;
	}
	
	if(tieBreakerState){
		var highestVoteCount = restaurantTally[0].voted.length;
		
		contendors = restaurantTally.filter(function(restaurant){
			return restaurant.voted.length == highestVoteCount;
		});
		
		res.send({restaurants:restaurantTally, tieBreakerState: true, contendors: contendors});
	} else {
		res.send({restaurants:restaurantTally, tieBreakerState: false});
	}
});*/

server.post('/api/Votes/clearAll', function(req, res){
	clearVotes();
});




var server_port = process.env.OPENSHIFT_NODEJS_PORT || 4649
var server_ip_address = process.env.OPENSHIFT_NODEJS_IP || '127.0.0.1'
 
server.listen(server_port, server_ip_address, function () {
	console.log( "Listening on " + server_ip_address + ", server_port " + server_port )
});

