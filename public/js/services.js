angular.module('LunchBot.Services', [])

.factory('RestaurantService', function($http){
	return {
		getAll: function(){
			return $http.get('/api/Restaurants/getAll');
		},
		create: function(restaurant){
			return $http.post('/api/Restaurants/create', restaurant);
		},
		nuke: function(restaurantName){
			return $http.delete('/api/Restaurants/delete?name=' + restaurantName);
		},
		save: function(){
			return $http.post('/api/Restaurants/save');
		}
	}
})

.factory('VoteService', function($http){
	return {
		vote: function(restaurant, voter){
			return $http.post('/api/Votes/vote?restaurantName=' + restaurant + '&voter=' + voter);
		},
		revoke: function(restaurant, voter){
			return $http.post('/api/Votes/revoke?restaurantName=' + restaurant + '&voter=' + voter);
		},
		/*finalize: function(){
			return $http.post('/api/Votes/finalize');
		},*/
		clear: function(){
			return $http.post('/api/Votes/clearAll');
		}
	}
});