angular.module('LunchBot.Controllers', [])

.controller('VotingCtrl', function($scope, RestaurantService, VoteService){
	
	var blankRestaurant = {
			name: "",
			voted: []
		};
		
	$scope.restaurants = [];
	$scope.voter = "";
	$scope.searchVal = "";
	$scope.viewTally = false;
	
	$scope.refreshList = function(){
		return RestaurantService.getAll()
			.then(function(res){
				$scope.restaurants = res.data;
				return $scope.restaurants;
			})
	}
	
	$scope.refreshList();
	
	$scope.$watch('voter', function(newVal, oldVal){
		$scope.voter = newVal.toLowerCase();
	})
	
	$scope.hasVoted = function(restaurant){
		return $scope.voter.length && restaurant.voted.indexOf($scope.voter) >= 0;
	}
	
	$scope.hasVotes = function(restaurant){
		return restaurant.voted.length > 0;
	}
	
	$scope.voteOrRevoke = function(restaurant){
		if(!$scope.voter.length){ return; }
		
		if($scope.hasVoted(restaurant)){
			$scope.revoke(restaurant);
		} else {
			$scope.vote(restaurant);
		}
	}
	
	$scope.vote = function(restaurant){
		VoteService.vote(restaurant.name, $scope.voter)
			.then(function(res){
				if(res.data.success){
					$scope.refreshList();
				}
			})
	}
	
	$scope.revoke = function(restaurant){
		VoteService.revoke(restaurant.name, $scope.voter)
			.then(function(res){
				if(res.data.success){
					$scope.refreshList();
				}
			})
	}
	
	$scope.searchByName = function(value, index, array){
		return !$scope.searchVal.length || (value.name && value.name.indexOf($scope.searchVal) >= 0);
	}
	
	$scope.saveRestaurants = function(){
		RestaurantService.save();
	}
	
	$scope.back = function(){
		$scope.viewTally = false;
	}
	
	$scope.showTally = function(){
		$scope.refreshList();
		$scope.viewTally = true;
	}
	
	$scope.sortByVotes = function(){
		var restaurantTally = restaurants.sort(function(a, b){
			return b.voted.length - a.voted.length;
		});
	}
	
	$scope.revote = function(){
		$scope.restaurants = $scope.contendors;
		
		$scope.restaurants.forEach(function(restaurant){
			restaurant.voted.length = 0;
		});
		
		$scope.viewTally = false;
	}
})
/*
controller('TallyCtrl', function($scope, RestaurantService, VoteService){
	VoteService.finalize()
		.then(function(res){
			$scope.tieBreakerState = res.data.tieBreakerState;
			if($scope.tieBreakerState){
				
			}
		})
});*/