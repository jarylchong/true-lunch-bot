angular.module('LunchBot', ['LunchBot.Controllers', 'LunchBot.Services', 'ngRoute'])

.config(['$routeProvider',
	function($routeProvider) {
	$routeProvider.
		when('/', {
			templateUrl: '/views/voting-view.html',
			controller: 'VotingCtrl'
		}).
	/*	when('/results', {
			templateUrl: '/views/tally-view.html',
			controller: 'TallyCtrl'
		}).*/
		otherwise({
			redirectTo: '/'
		});
	}]);