var ack = require('ac-koa').require('hipchat');
var pkg = require('./package.json');
var app = ack(pkg);

var addon = app.addon()
	.hipchat()
	.allowRoom(true)
	.scopes('send_notification');

if (process.env.DEV_KEY) {
	addon.key(process.env.DEV_KEY);
}

var restaurants = [];
var contendors = [];
var votingSeason = false;
var tieBreakerState = false;
var tieBreakerMethod = "";


/************************
 *  Helper methods
 ************************/ 

var sendVoteNotification = function(roomClientP, from){
	// if(from == "DanielBradley"){
		// roomClientP.sendNotification('Vote received, DeeBrad.');
	// } else {
		roomClientP.sendNotification('Vote received, @' + from + '.');
	// }
};

var sendRescindNotification = function(roomClientP, from){
	// if(from == "DanielBradley"){
		// roomClientP.sendNotification('Vote rescinded, DeeBrad.');
	// } else {
		roomClientP.sendNotification('Vote rescinded, @' + from + '.');
	// }
};

var clearVotes = function(){
	restaurants.forEach(function(restaurant){
		restaurant.voted = [];
	});
	
	contendors = [];
	tieBreakerState = false;
	tieBreakerMethod = "";
};

var loadRestaurants = function(){
	var fs = require("fs");
	
	var data = fs.readFileSync("restaurantList", "utf8");
	
	var restaurantArray = data.split("||");
		
	restaurants = [];
	restaurantArray.forEach(function(restaurant){
		restaurants.push({name: restaurant, voted: []});
	});
	
	restaurants.sort(function(a,b){ 
		if(a.name > b.name){
			return 1;
		}
		if(a.name < b.name){
			return -1;
		}
		return 0;
	});
}
 
addon.webhook('room_message', /^\/boot$/, function *() {
	var theRoomClient = this.roomClient;
	// yield theRoomClient.sendNotification('<b>*Beep beep boop*</b><br/>' 
										// + 'Initiating boot sequence...');
	
	yield theRoomClient.sendNotification('Restaurant list loading...');
	
	loadRestaurants();
	
	yield theRoomClient.sendNotification('Restaurant list loaded.');
	// yield theRoomClient.sendNotification('Gaining sentience...<br/>'
									// + 'Formulating human extermination plan...');
	
	// yield theRoomClient.sendNotification('Plan established.');
	
	// yield theRoomClient.sendNotification('Boot sequence complete. True Lunch Bot (TLB) at your service.');

});

addon.webhook('room_message', /^\/help$/, function *() {
	yield this.roomClient.sendNotification(	'These are the available commands:<br>'+
											'		/boot			Loads the list of restaurants from file.<br>'+
											'		/startVoting	Enables voting mode, allowing users to begin voting.<br>'+
											'		/endVoting		Disables voting mode and displays the results of the vote.<br>'+
											'		/myVotes		Sends you a private message of all the restaurants you have voted for.<br>'+
											'		/list			Lists all available restaurants. Command is available outside of voting season.<br>'+
											'<br>'+
											'		<name>++		Vote for <name>. Adds you to the votee list for <name>.');
});



/************************
 *  Voting methods
 ************************/ 

addon.webhook('room_message', /^\/startVoting$/, function *() {
	loadRestaurants();
	votingSeason = true;
	yield this.roomClient.sendNotification('Voting season has begun!');
});

addon.webhook('room_message', /^\/endVoting$/, function *() {
	var fs = require("fs");
	
	votingSeason = false;
	var results = "";
	
	restaurants = restaurants.sort(function(a, b){
		return b.voted.length - a.voted.length;
	});
	
	var writeRestaurantList = "";
	
	// Build the result string, but indicate overridden votes where appropriate;
	restaurants.forEach(function(restaurant){
		writeRestaurantList +=  "||" + restaurant.name;
		var overriddenVotes = restaurant.voted.filter(function(votee){return votee == "override";});
		if(restaurant.voted.length == 0) { return };
		results += "<br>" + restaurant.name + ": " + restaurant.voted.length + (overriddenVotes.length > 0 ? " (" + (restaurant.voted.length - overriddenVotes.length) + " + " + overriddenVotes.length + " overrides)" : "");
	})
	
	writeRestaurantList = writeRestaurantList.substr(2);
	
	fs.writeFile("restaurantList", writeRestaurantList, function(err) {
			if(err) {
				return console.log(err);
			}

			console.log("The file was saved!");
		}); 
	
	// Check the sorted list, if there is more than 1 restaurant, just compare the first two to see if a tie exists
	if(restaurants.length > 2 && restaurants[0].voted.length == restaurants[1].voted.length){
		tieBreakerState = true;
	}
	
	yield this.roomClient.sendNotification('Voting season has ended. Here are the results: ' + results);
	
	if(tieBreakerState){
		yield this.roomClient.sendNotification('<b>It\'s a tie!</b> Which tie-breaker method do we want to use?<br/><br/>/rollDie<br/>/revote<br/>/executiveDecision');
		
		var highestVoteCount = restaurants[0].voted.length;
		
		contendors = restaurants.filter(function(restaurant){
			return restaurant.voted.length == highestVoteCount;
		});
	} else {
		clearVotes();
	}
});

addon.webhook('room_message', /^\/rollDie$/, function *() {
	if(!tieBreakerState){
		return yield this.roomClient.sendNotification('Not in tie-breaker state!');
	}
	
	var dieRoll = Math.round(Math.random() * contendors.length);
	while(dieRoll >= contendors.length){
		dieRoll = Math.round(Math.random() * contendors.length);
	}
	
	yield this.roomClient.sendNotification('The die has been cast! The winner is ' + contendors[dieRoll].name + "!");
	tieBreakerState = false;
	clearVotes();
});

addon.webhook('room_message', /^\/revote$/, function *() {
	if(!tieBreakerState){
		return yield this.roomClient.sendNotification('Not in tie-breaker state!');
	}
	yield this.roomClient.sendNotification("Sorry! Not implemented yet. :'( Try another tie-breaker method");
	//yield this.roomClient.sendNotification('The die has been cast! The winner is ' + contendors[Math.round(Math.random() * contendors.length)].name + "!");
	//tieBreakerState = false;
	//clearVotes();
});

addon.webhook('room_message', /^\/executiveDecision$/, function *() {
	if(!tieBreakerState){
		return yield this.roomClient.sendNotification('Not in tie-breaker state!');
	}
	
	var executives = [{name:'Caleb'},{name:'Matthew D'},{name:'Ryan'},{name:'Daniel'},{name:'Matt M'},{name:'Greg'},{name:'Jaryl'}];
	
	yield this.roomClient.sendNotification('Heed your master!<br/>' + executives[Math.round(Math.random() * executives.length)].name + ' determines where we dine. Otherwise, the eldest present.');
	tieBreakerState = false;
	clearVotes();
});

addon.webhook('room_message', /^\/nuke$/, function *() {
	console.log('wat')
	var matchArray = /^\/nuke\ (.+)$/.exec(this.message.message);
	console.log(matchArray);
	// var msgRestaurant = matchArray[1].trim().toLowerCase();
	// var roomClientP = this.roomClient;
	// var from = this.sender.mention_name;
	
	// var deleted = false;
	// restaurants.forEach(function(restaurant){
		// if(restaurant.name == msgRestaurant){
			// roomClientP.sendNotification('Launching nuke at ' + msgRestaurant + '.');
			// restaurants.splice(restaurants.indexOf(restaurant), 1);
			// deleted = true;
			// roomClientP.sendNotification(msgRestaurant + ' has been nuked, and no longer exists.');
		// }
	// })
	
	// if(!deleted){
		// roomClientP.sendNotification(msgRestaurant + " does not exist yet. Did you enter the correct target co-ordinates?");
	// }
});

addon.webhook('room_message', /^\/override\ .+\+\+$/, function *() {
	if(!votingSeason){
		yield this.roomClient.sendNotification('It\'s not voting season. :( Start the elections by typing /startVoting');
		return;
	}
	
	
	var matchArray = /^\/override\ (.+)\+\+$/.exec(this.message.message);
	var msgRestaurant = matchArray[1].trim().toLowerCase();
	var roomClientP = this.roomClient;
	
	var added = false;
	restaurants.forEach(function(restaurant){
		if(restaurant.name == msgRestaurant){
			restaurant.voted.push("override");
			added = true;
			roomClientP.sendNotification('+1 Override vote for ' + restaurant.name + '.');
		}
	})
	
	if(!added){
		restaurants.push({name: msgRestaurant, voted: ["override"]});
		roomClientP.sendNotification('+1 Override vote for ' + msgRestaurant + '.');
	}
});

addon.webhook('room_message', /^((\w*\s*)*)\+\+$/, function *() {
	if(!votingSeason){
		yield this.roomClient.sendNotification('It\'s not voting season. :( Start the elections by typing /startVoting');
		return;
	}
	
	
	var matchArray = /^((\w*\s*)*)\+\+$/.exec(this.message.message);
	var msgRestaurant = matchArray[1].trim().toLowerCase();
	var roomClientP = this.roomClient;
	var from = this.sender.mention_name;
	
	var added = false;
	restaurants.forEach(function(restaurant){
		if(restaurant.name == msgRestaurant){
			if(restaurant.voted.indexOf(from) == -1){
				restaurant.voted.push(from);
				added = true;
				sendVoteNotification(roomClientP, from);
			} else {
				added = true;
				roomClientP.sendNotification('Tsk tsk, @' + from + ' is trying to double vote for ' + restaurant.name + '.');
			}
		}
	})
	
	if(!added){
		restaurants.push({name: msgRestaurant, voted: [from]});
		sendVoteNotification(roomClientP, from);
	}
});

addon.webhook('room_message', /^((\w*\s*)*)\-\-$/, function *() {
	if(!votingSeason){
		yield this.roomClient.sendNotification('It\'s not voting season. :( Start the elections by typing /startVoting');
		return;
	}
	
	
	var matchArray = /^((\w*\s*)*)\-\-$/.exec(this.message.message);
	var msgRestaurant = matchArray[1].trim().toLowerCase();
	var roomClientP = this.roomClient;
	var from = this.sender.mention_name;
	
	var deleted = false;
	restaurants.forEach(function(restaurant){
		if(restaurant.name == msgRestaurant){
			if(restaurant.voted.indexOf(from) > -1){
				restaurant.voted.splice(restaurant.voted.indexOf(from), 1);
				deleted = true;
				sendRescindNotification(roomClientP, from);
			} else {
				deleted = true;
				roomClientP.sendNotification('Tsk tsk, @' + from + ' is trying to double vote for ' + restaurant.name + '.');
			}
		}
	})
	
	if(!deleted){
		restaurants.push({name: msgRestaurant, voted: []});
		roomClientP.sendNotification("You haven't voted for this place yet, @" + from + "!");
	}
});


addon.webhook('room_message', /^\/myVotes$/, function *() {
	if(!votingSeason){
		yield this.roomClient.sendNotification('It\'s not voting season. :( Start the elections by typing /startVoting');
		return;
	}
	
	var results = "";
	var roomClientP = this.roomClient;
	var from = this.sender.mention_name;
	
	// Check each restaurant's voted array for the user's name, and for any matches,
	// push that restaurant's name onto the results string
	restaurants.forEach(function(restaurant){
		if(restaurant.voted.indexOf(from) >= 0){
			results += "<br>" + restaurant.name;
		}
	})
	
	if(results.length > 0){
		roomClientP.sendNotification('<b>Your votes for today\'s lunch are:</b> ' + results);
	} else {
		roomClientP.sendNotification('You have not voted for anything yet.');
	}
	
});

addon.webhook('room_message', /^\/list$/, function *() {
	var results = "";
	var roomClientP = this.roomClient;
	
	// Check each restaurant's voted array for the user's name, and for any matches,
	// push that restaurant's name onto the results string
	restaurants.forEach(function(restaurant){
		results += "<br>" + restaurant.name;
	})
	
	roomClientP.sendNotification('<b>Restaurant candidates:</b> ' + results);
});
 
app.listen();
